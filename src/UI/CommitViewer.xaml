<UserControl x:Class="SourceGit.UI.CommitViewer"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             xmlns:source="clr-namespace:SourceGit"
             xmlns:local="clr-namespace:SourceGit.UI"
             xmlns:git="clr-namespace:SourceGit.Git"
             xmlns:converters="clr-namespace:SourceGit.Converters"
             xmlns:helpers="clr-namespace:SourceGit.Helpers"
             mc:Ignorable="d" 
             d:DesignHeight="450" d:DesignWidth="800"
             Unloaded="Cleanup">
    <UserControl.Resources>
        <Style x:Key="Style.DataGridRow.NoBringIntoView" TargetType="{x:Type DataGridRow}" BasedOn="{StaticResource Style.DataGridRow}">
            <EventSetter Event="RequestBringIntoView" Handler="OnPreviewRequestBringIntoView"/>
        </Style>

        <Style x:Key="Style.DataGridText.LineNumber" TargetType="{x:Type TextBlock}">
            <Setter Property="HorizontalAlignment" Value="Right"/>
            <Setter Property="VerticalAlignment" Value="Center"/>
            <Setter Property="Padding" Value="8,0"/>
            <Setter Property="FontSize" Value="12"/>
        </Style>

        <Style x:Key="Style.DataGridText.Content" TargetType="{x:Type TextBlock}">
            <Setter Property="HorizontalAlignment" Value="Left"/>
            <Setter Property="VerticalAlignment" Value="Center"/>
            <Setter Property="Padding" Value="4,0,0,0"/>
            <Setter Property="FontSize" Value="12"/>
        </Style>
    </UserControl.Resources>
    
    <TabControl>
        <TabItem Header="{StaticResource Text.CommitViewer.Info}">
            <Grid>
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="*"/>
                </Grid.RowDefinitions>
                
                <!-- Author and committer -->
                <Grid Grid.Row="0" Margin="0,8">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*"/>
                        <ColumnDefinition Width="*"/>
                    </Grid.ColumnDefinitions>
                    
                    <!-- Author -->
                    <Grid Grid.Column="0">
                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto"/>
                            <RowDefinition Height="Auto"/>
                            <RowDefinition Height="Auto"/>
                        </Grid.RowDefinitions>

                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="96"/>
                            <ColumnDefinition Width="*"/>
                        </Grid.ColumnDefinitions>

                        <Border Grid.Row="0" Grid.RowSpan="3" Grid.Column="0" x:Name="authorAvatarMask"  Width="64" Height="64" HorizontalAlignment="Right" Background="Gray">
                            <Path Style="{StaticResource Style.Icon}" Fill="White" Width="56" Height="56" Data="{StaticResource Icon.User}" VerticalAlignment="Bottom"/>
                        </Border>

                        <Image Grid.Row="0" Grid.RowSpan="3" Grid.Column="0" Width="64" Height="64" x:Name="authorAvatar" HorizontalAlignment="Right"/>

                        <TextBlock Grid.Row="0" Grid.Column="1" Margin="15,0,0,0" Text="{StaticResource Text.CommitViewer.Info.Author}" Opacity=".6" Foreground="{StaticResource Brush.FG1}"/>
                        <StackPanel Grid.Row="1" Grid.Column="1" Orientation="Horizontal" Margin="12,0,0,0">
                            <TextBox x:Name="authorName" IsReadOnly="True" Background="Transparent" Foreground="{StaticResource Brush.FG1}" BorderThickness="0"/>
                            <TextBox x:Name="authorEmail" Margin="4,0,0,0" IsReadOnly="True" Background="Transparent" Foreground="{StaticResource Brush.FG2}" BorderThickness="0"/>
                        </StackPanel>
                        <TextBox Grid.Row="2" Grid.Column="1" Margin="12,0,0,0" x:Name="authorTime" IsReadOnly="True" Background="Transparent" Foreground="{StaticResource Brush.FG2}" FontSize="11" BorderThickness="0"/>
                    </Grid>

                    <!-- Committer -->
                    <Grid Grid.Column="1" x:Name="committerPanel">
                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto"/>
                            <RowDefinition Height="Auto"/>
                            <RowDefinition Height="Auto"/>
                        </Grid.RowDefinitions>

                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="96"/>
                            <ColumnDefinition Width="*"/>
                        </Grid.ColumnDefinitions>

                        <Border Grid.Row="0" Grid.RowSpan="3" Grid.Column="0" x:Name="committerAvatarMask" Width="64" Height="64" HorizontalAlignment="Right" Background="Gray">
                            <Path Style="{StaticResource Style.Icon}" Fill="White" Width="56" Height="56" Data="{StaticResource Icon.User}" VerticalAlignment="Bottom"/>
                        </Border>

                        <Image Grid.Row="0" Grid.RowSpan="3" Grid.Column="0" Width="64" Height="64" x:Name="committerAvatar" HorizontalAlignment="Right"/>

                        <TextBlock Grid.Row="0" Grid.Column="1" Margin="15,0,0,0" Text="{StaticResource Text.CommitViewer.Info.Committer}" Opacity=".6" Foreground="{StaticResource Brush.FG1}"/>
                        <StackPanel Grid.Row="1" Grid.Column="1" Margin="12,0,0,0" Orientation="Horizontal">
                            <TextBox x:Name="committerName" IsReadOnly="True" Background="Transparent" Foreground="{StaticResource Brush.FG1}" BorderThickness="0"/>
                            <TextBox x:Name="committerEmail" Margin="4,0,0,0" IsReadOnly="True" Background="Transparent" Foreground="{StaticResource Brush.FG2}" BorderThickness="0"/>
                        </StackPanel>
                        <TextBox Grid.Row="2" Grid.Column="1" Margin="12,0,0,0" x:Name="committerTime" IsReadOnly="True" Background="Transparent" Foreground="{StaticResource Brush.FG2}" FontSize="11" BorderThickness="0"/>
                    </Grid>
                </Grid>

                <Rectangle Grid.Row="1" Height="1" Margin="8" Fill="{StaticResource Brush.Border2}"/>

                <!-- Base information -->
                <Grid Grid.Row="2">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition x:Name="refRow" Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition x:Name="descRow" Height="Auto"/>
                    </Grid.RowDefinitions>

                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="96"/>
                        <ColumnDefinition Width="*"/>
                    </Grid.ColumnDefinitions>

                    <!-- SHA -->
                    <Label Grid.Row="0" Grid.Column="0" Content="{StaticResource Text.CommitViewer.Info.SHA}" HorizontalAlignment="Right" Opacity=".6"/>
                    <TextBox 
                        Grid.Row="0" Grid.Column="1" 
                        x:Name="SHA" 
                        IsReadOnly="True" 
                        Background="Transparent" 
                        FontFamily="Consolas"
                        BorderThickness="0"
                        Margin="11,0,0,0"/>

                    <!-- PARENTS -->
                    <Label Grid.Row="1" Grid.Column="0" Content="{StaticResource Text.CommitViewer.Info.Parents}" HorizontalAlignment="Right" Opacity=".6"/>
                    <ItemsControl Grid.Row="1" Grid.Column="1" x:Name="parents" Margin="8,0,0,0">
                        <ItemsControl.ItemsPanel>
                            <ItemsPanelTemplate>
                                <VirtualizingStackPanel Orientation="Horizontal" VerticalAlignment="Center"/>
                            </ItemsPanelTemplate>
                        </ItemsControl.ItemsPanel>

                        <ItemsControl.ItemTemplate>
                            <DataTemplate>
                                <Label Margin="0,0,8,0" FontFamily="Consolas">
                                    <Hyperlink RequestNavigate="NavigateParent" NavigateUri="{Binding .}" ToolTip="{StaticResource Text.Goto}">
                                        <Run Text="{Binding .}"/>
                                    </Hyperlink>
                                </Label>
                            </DataTemplate>
                        </ItemsControl.ItemTemplate>
                    </ItemsControl>

                    <!-- Refs -->
                    <Label Grid.Row="2" Grid.Column="0" Content="{StaticResource Text.CommitViewer.Info.Refs}" HorizontalAlignment="Right" Opacity=".6"/>
                    <ItemsControl Grid.Row="2" Grid.Column="1" x:Name="refs" Margin="11,0,0,0">
                        <ItemsControl.ItemsPanel>
                            <ItemsPanelTemplate>
                                <VirtualizingStackPanel Orientation="Horizontal" VerticalAlignment="Center"/>
                            </ItemsPanelTemplate>
                        </ItemsControl.ItemsPanel>

                        <ItemsControl.ItemTemplate>
                            <DataTemplate>
                                <Border x:Name="BG" Height="16" Margin="2">
                                    <Grid>
                                        <Grid.ColumnDefinitions>
                                            <ColumnDefinition Width="18"/>
                                            <ColumnDefinition Width="Auto"/>
                                        </Grid.ColumnDefinitions>

                                        <Border Grid.Column="0" Background="{StaticResource Brush.Decorator}">
                                            <Path x:Name="Icon" Width="8" Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.Branch}"/>
                                        </Border>

                                        <Label x:Name="Name" Grid.Column="1" Content="{Binding Name}" FontSize="11" Padding="4,0" Foreground="Black"/>
                                    </Grid>
                                </Border>

                                <DataTemplate.Triggers>
                                    <DataTrigger Binding="{Binding Type}" Value="{x:Static git:DecoratorType.Tag}">
                                        <Setter TargetName="BG" Property="Background" Value="#FF02C302"/>
                                        <Setter TargetName="Icon" Property="Data" Value="{StaticResource Icon.Tag}"/>
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding Type}" Value="{x:Static git:DecoratorType.LocalBranchHead}">
                                        <Setter TargetName="BG" Property="Background" Value="#FFFFB835"/>
                                        <Setter TargetName="Icon" Property="Data" Value="{StaticResource Icon.Branch}"/>
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding Type}" Value="{x:Static git:DecoratorType.RemoteBranchHead}">
                                        <Setter TargetName="BG" Property="Background" Value="#FFFFB835"/>
                                        <Setter TargetName="Icon" Property="Data" Value="{StaticResource Icon.Remote}"/>
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding Type}" Value="{x:Static git:DecoratorType.CurrentBranchHead}">
                                        <Setter TargetName="BG" Property="Background" Value="#FFFFB835"/>
                                        <Setter TargetName="Icon" Property="Data" Value="{StaticResource Icon.Check}"/>
                                        <Setter TargetName="Icon" Property="Fill" Value="Orange"/>
                                    </DataTrigger>
                                </DataTemplate.Triggers>
                            </DataTemplate>
                        </ItemsControl.ItemTemplate>
                    </ItemsControl>

                    <!-- MESSAGE -->
                    <Label Grid.Row="3" Grid.Column="0" Content="{StaticResource Text.CommitViewer.Info.Message}" HorizontalAlignment="Right" Opacity=".6"/>
                    <TextBox 
                        Grid.Row="3" Grid.Column="1"
                        x:Name="subject"
                        IsReadOnly="True"
                        Background="Transparent"
                        FontFamily="Consolas"
                        BorderThickness="0"
                        Margin="11,0,16,0"/>
                    <TextBox 
                        Grid.Row="4" Grid.Column="1"
                        x:Name="message"
                        IsReadOnly="True"
                        Background="Transparent"
                        BorderThickness="0"
                        FontSize="11"
                        Margin="11,8,0,0"/>
                </Grid>

                <Rectangle Grid.Row="3" Grid.ColumnSpan="2" Height="1" Margin="8" Fill="{StaticResource Brush.Border2}"/>

                <!-- Changes -->
                <Grid Grid.Row="4">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="96"/>
                        <ColumnDefinition Width="*"/>
                    </Grid.ColumnDefinitions>

                    <Label Grid.Column="0" Content="{StaticResource Text.CommitViewer.Info.Changed}" HorizontalAlignment="Right" VerticalAlignment="Top" Opacity=".6"/>
                    <DataGrid
                        Grid.Column="1"
                        x:Name="changeList1"
                        RowHeight="20"
                        Margin="11,2,0,2">
                        <DataGrid.Resources>
                            <converters:FileStatusToColor x:Key="StatusColorConverter"/>
                            <converters:FileStatusToIcon x:Key="StatusIconConverter"/>

                            <Style x:Key="Style.DataGridText.VerticalCenter" TargetType="{x:Type TextBlock}">
                                <Setter Property="VerticalAlignment" Value="Center"/>
                            </Style>
                        </DataGrid.Resources>

                        <DataGrid.Columns>
                            <DataGridTemplateColumn Width="22">
                                <DataGridTemplateColumn.CellTemplate>
                                    <DataTemplate>
                                        <Border Width="14" Height="14" x:Name="status" Background="{Binding ., Converter={StaticResource StatusColorConverter}}" CornerRadius="2" Margin="2,0,4,0">
                                            <TextBlock Text="{Binding ., Converter={StaticResource StatusIconConverter}}" Foreground="{StaticResource Brush.FG1}" TextAlignment="Center" VerticalAlignment="Center" FontSize="8"/>
                                        </Border>
                                    </DataTemplate>
                                </DataGridTemplateColumn.CellTemplate>
                            </DataGridTemplateColumn>
                            <DataGridTextColumn Width="*" Binding="{Binding Path}" Foreground="{StaticResource Brush.FG1}" FontFamily="Consolas" ElementStyle="{StaticResource Style.DataGridText.VerticalCenter}"/>
                        </DataGrid.Columns>

                        <DataGrid.RowStyle>
                            <Style TargetType="{x:Type DataGridRow}" BasedOn="{StaticResource Style.DataGridRow}">
                                <EventSetter Event="ContextMenuOpening" Handler="ChangeListContextMenuOpening"/>
                                <EventSetter Event="MouseDoubleClick" Handler="ChangeListMouseDoubleClick"/>
                            </Style>
                        </DataGrid.RowStyle>
                    </DataGrid>
                </Grid>
            </Grid>
        </TabItem>

        <!-- CHANGES -->
        <TabItem Header="{StaticResource Text.CommitViewer.Changes}">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="200" MinWidth="200"/>
                    <ColumnDefinition Width="1"/>
                    <ColumnDefinition Width="*"/>
                </Grid.ColumnDefinitions>

                <Grid Grid.Column="0" Margin="2,0">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="24"/>
                        <RowDefinition Height="*"/>
                    </Grid.RowDefinitions>

                    <Grid.Resources>
                        <converters:BoolToCollapsed x:Key="BoolToCollapsed"/>
                        <converters:InverseBoolToCollapsed x:Key="InverseBoolToCollapsed"/>
                    </Grid.Resources>

                    <Grid Grid.Row="0" Margin="0,0,0,4">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="24"/>
                            <ColumnDefinition Width="*"/>
                            <ColumnDefinition Width="24"/>
                        </Grid.ColumnDefinitions>

                        <Border Grid.Column="0" Grid.ColumnSpan="2" BorderThickness="1" BorderBrush="{StaticResource Brush.Border2}"/>
                        <Path Grid.Column="0" Width="14" Height="14" Fill="{StaticResource Brush.FG2}" Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.Search}"/>
                        <TextBox Grid.Column="1" x:Name="txtChangeFilter" BorderThickness="0" helpers:TextBoxHelper.Placeholder="{StaticResource Text.CommitViewer.Changes.Search}" TextChanged="SearchChangeFileTextChanged"/>
                        <ToggleButton
                            Grid.Column="2" 
                            x:Name="toggleSwitchMode"
                            Margin="4,0,0,0" 
                            ToolTip="{StaticResource Text.CommitViewer.Changes.SwitchMode}"
                            Style="{StaticResource Style.ToggleButton.ListOrTree}" 
                            IsChecked="{Binding Source={x:Static source:App.Setting}, Path=UI.UseListInChanges, Mode=TwoWay}"/>
                    </Grid>

                    <TreeView
                        Grid.Row="1" 
                        x:Name="changeTree" 
                        FontFamily="Consolas" 
                        Visibility="{Binding ElementName=toggleSwitchMode, Path=IsChecked, Converter={StaticResource InverseBoolToCollapsed}}"
                        Background="{StaticResource Brush.Contents}"
                        SelectedItemChanged="ChangeTreeItemSelected" 
                        PreviewMouseWheel="TreeMouseWheel">
                        <TreeView.Resources>
                            <converters:FileStatusToColor x:Key="StatusColorConverter"/>
                            <converters:FileStatusToIcon x:Key="StatusIconConverter"/>
                        </TreeView.Resources>
                        <TreeView.ItemContainerStyle>
                            <Style TargetType="{x:Type TreeViewItem}" BasedOn="{StaticResource Style.TreeView.ItemContainerStyle}">
                                <Setter Property="IsExpanded" Value="{Binding IsNodeExpanded, Mode=TwoWay}"/>
                                <EventSetter Event="ContextMenuOpening" Handler="TreeContextMenuOpening"/>
                                <EventSetter Event="RequestBringIntoView" Handler="TreeRequestBringIntoView"/>
                            </Style>
                        </TreeView.ItemContainerStyle>
                        <TreeView.ItemTemplate>
                            <HierarchicalDataTemplate ItemsSource="{Binding Children}">
                                <StackPanel Orientation="Horizontal" Height="24">
                                    <Border x:Name="status" Width="14" Height="14" Visibility="Collapsed" Background="{Binding Change, Converter={StaticResource StatusColorConverter}}" CornerRadius="2" Margin="0,0,4,0">
                                        <TextBlock Text="{Binding Change, Converter={StaticResource StatusIconConverter}}" Foreground="{StaticResource Brush.FG1}" TextAlignment="Center" VerticalAlignment="Center" FontSize="10" RenderOptions.BitmapScalingMode="HighQuality"/>
                                    </Border>
                                    <Path x:Name="icon" Width="14" Style="{StaticResource Style.Icon}" Fill="Goldenrod" Data="{StaticResource Icon.Folder.Fill}"/>
                                    <TextBlock Text="{Binding Name}" Foreground="{StaticResource Brush.FG1}" TextAlignment="Center" VerticalAlignment="Center" Margin="4,0,0,0" FontSize="11"/>
                                </StackPanel>

                                <HierarchicalDataTemplate.Triggers>
                                    <DataTrigger Binding="{Binding IsFile}" Value="True">
                                        <Setter TargetName="status" Property="Visibility" Value="Visible"/>
                                        <Setter TargetName="icon" Property="Visibility" Value="Collapsed"/>
                                    </DataTrigger>
                                    <MultiDataTrigger>
                                        <MultiDataTrigger.Conditions>
                                            <Condition Binding="{Binding IsFile}" Value="False"/>
                                            <Condition Binding="{Binding IsNodeExpanded}" Value="True"/>
                                        </MultiDataTrigger.Conditions>
                                        <Setter TargetName="icon" Property="Data" Value="{StaticResource Icon.Folder.Open}"/>
                                    </MultiDataTrigger>
                                </HierarchicalDataTemplate.Triggers>
                            </HierarchicalDataTemplate>
                        </TreeView.ItemTemplate>
                    </TreeView>

                    <DataGrid
                        Grid.Row="1"
                        x:Name="changeList2"
                        Visibility="{Binding ElementName=toggleSwitchMode, Path=IsChecked, Converter={StaticResource BoolToCollapsed}}"
                        RowHeight="24"
                        SelectionChanged="ChangeListSelectionChanged"
                        SelectionMode="Single"
                        SelectionUnit="FullRow"
                        Background="{StaticResource Brush.Contents}">
                        <DataGrid.Resources>
                            <converters:FileStatusToColor x:Key="StatusColorConverter"/>
                            <converters:FileStatusToIcon x:Key="StatusIconConverter"/>

                            <Style x:Key="Style.DataGridText.VerticalCenter" TargetType="{x:Type TextBlock}">
                                <Setter Property="VerticalAlignment" Value="Center"/>
                            </Style>
                        </DataGrid.Resources>

                        <DataGrid.Columns>
                            <DataGridTemplateColumn Width="22">
                                <DataGridTemplateColumn.CellTemplate>
                                    <DataTemplate>
                                        <Border Width="14" Height="14" x:Name="status" Background="{Binding ., Converter={StaticResource StatusColorConverter}}" CornerRadius="2" Margin="2,0,4,0">
                                            <TextBlock Text="{Binding ., Converter={StaticResource StatusIconConverter}}" Foreground="{StaticResource Brush.FG1}" TextAlignment="Center" VerticalAlignment="Center" FontSize="8"/>
                                        </Border>
                                    </DataTemplate>
                                </DataGridTemplateColumn.CellTemplate>
                            </DataGridTemplateColumn>
                            <DataGridTextColumn Width="*" Binding="{Binding Path}" Foreground="{StaticResource Brush.FG1}" FontFamily="Consolas" ElementStyle="{StaticResource Style.DataGridText.VerticalCenter}"/>
                        </DataGrid.Columns>

                        <DataGrid.RowStyle>
                            <Style TargetType="{x:Type DataGridRow}" BasedOn="{StaticResource Style.DataGridRow}">
                                <EventSetter Event="ContextMenuOpening" Handler="ChangeListContextMenuOpening"/>
                            </Style>
                        </DataGrid.RowStyle>
                    </DataGrid>
                </Grid>

                <GridSplitter Grid.Column="1" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" Background="Transparent"/>

                <local:DiffViewer Grid.Column="2" x:Name="diffViewer"/>
            </Grid>
        </TabItem>

        <!-- FILE TREE -->
        <TabItem Header="{StaticResource Text.CommitViewer.Files}">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="200" MinWidth="200" MaxWidth="400"/>
                    <ColumnDefinition Width="1"/>
                    <ColumnDefinition Width="*"/>
                </Grid.ColumnDefinitions>

                <Border Grid.Column="0" Margin="2" Background="{StaticResource Brush.Contents}">
                    <TreeView x:Name="fileTree" SelectedItemChanged="FileTreeItemSelected" FontFamily="Consolas" PreviewMouseWheel="TreeMouseWheel">
                        <TreeView.ItemContainerStyle>
                            <Style TargetType="{x:Type TreeViewItem}" BasedOn="{StaticResource Style.TreeView.ItemContainerStyle}">
                                <Setter Property="IsExpanded" Value="{Binding IsNodeExpanded, Mode=TwoWay}"/>
                                <EventSetter Event="ContextMenuOpening" Handler="TreeContextMenuOpening"/>
                                <EventSetter Event="RequestBringIntoView" Handler="TreeRequestBringIntoView"/>
                            </Style>
                        </TreeView.ItemContainerStyle>
                        <TreeView.ItemTemplate>
                            <HierarchicalDataTemplate ItemsSource="{Binding Children}">
                                <StackPanel Orientation="Horizontal" Height="24">
                                    <Path x:Name="icon" Width="14" Style="{StaticResource Style.Icon}" Fill="Goldenrod" Data="{StaticResource Icon.Folder.Fill}"/>
                                    <TextBlock Text="{Binding Name}" Foreground="{StaticResource Brush.FG1}" TextAlignment="Center" VerticalAlignment="Center" Margin="6,0,0,0" FontSize="11"/>
                                </StackPanel>

                                <HierarchicalDataTemplate.Triggers>
                                    <DataTrigger Binding="{Binding IsFile}" Value="True">
                                        <Setter TargetName="icon" Property="Data" Value="{StaticResource Icon.File}"/>
                                        <Setter TargetName="icon" Property="Fill" Value="{StaticResource Brush.FG1}"/>
                                        <Setter TargetName="icon" Property="Opacity" Value=".75"/>
                                    </DataTrigger>
                                    <MultiDataTrigger>
                                        <MultiDataTrigger.Conditions>
                                            <Condition Binding="{Binding IsFile}" Value="False"/>
                                            <Condition Binding="{Binding IsNodeExpanded}" Value="True"/>
                                        </MultiDataTrigger.Conditions>
                                        <Setter TargetName="icon" Property="Data" Value="{StaticResource Icon.Folder.Open}"/>
                                    </MultiDataTrigger>
                                </HierarchicalDataTemplate.Triggers>
                            </HierarchicalDataTemplate>
                        </TreeView.ItemTemplate>
                    </TreeView>
                </Border>

                <GridSplitter Grid.Column="1" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" Background="Transparent"/>

                <Border Grid.Column="2" BorderThickness="1" Margin="2,0" BorderBrush="{StaticResource Brush.Border2}">
                    <Grid>
                        <Grid x:Name="previewEditor" SizeChanged="OnPreviewSizeChanged" TextElement.FontFamily="Consolas" TextElement.Foreground="{StaticResource Brush.FG1}"/>

                        <ScrollViewer x:Name="previewImage" HorizontalScrollBarVisibility="Auto" VerticalScrollBarVisibility="Auto" HorizontalAlignment="Center" VerticalAlignment="Center">
                            <Image x:Name="previewImageData" Width="Auto" Height="Auto"/>
                        </ScrollViewer>

                        <StackPanel x:Name="maskRevision" Orientation="Vertical" VerticalAlignment="Center" HorizontalAlignment="Center" Visibility="Collapsed">
                            <Path x:Name="iconPreviewRevision" Width="64" Height="64" Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.Submodule}" Fill="{StaticResource Brush.FG2}"/>
                            <Label x:Name="txtPreviewRevision" Margin="0,16,0,0" FontFamily="Consolas" FontSize="18" FontWeight="UltraBold" HorizontalAlignment="Center" Foreground="{StaticResource Brush.FG2}"/>
                        </StackPanel>

                        <StackPanel x:Name="maskPreviewNotSupported" Orientation="Vertical" VerticalAlignment="Center" HorizontalAlignment="Center" Visibility="Collapsed">
                            <Path Width="64" Height="64" Style="{StaticResource Style.Icon}" Data="{StaticResource Icon.Info}" Fill="{StaticResource Brush.FG2}"/>
                            <Label Margin="0,16,0,0" Content="{StaticResource Text.BinaryNotSupported}" FontFamily="Consolas" FontSize="18" FontWeight="UltraBold" HorizontalAlignment="Center" Foreground="{StaticResource Brush.FG2}"/>
                        </StackPanel>
                    </Grid>
                </Border>
            </Grid>
        </TabItem>
    </TabControl>
</UserControl>
