# SourceGit

开源的Git客户端，仅用于Windows。

## 下载

[发行版](https://gitee.com/sourcegit/SourceGit/releases/)

## 预览

* DarkTheme

![Theme Dark](./screenshots/theme_dark.png)

* LightTheme

![Theme Light](./screenshots/theme_light.png)


## Thanks

* [PUMA](https://gitee.com/whgfu) 配置默认User
